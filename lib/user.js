function User( data ){
	this._id = data._id;
	this.username = data.username;	
	this.display_name = data.display_name;
	this.permissions = data.permissions;
}

module.exports = User;