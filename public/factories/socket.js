(function() {

    NewsFeed.factory("socket", [
        "socketFactory", 
        function(socketFactory) {
            // establish
            var ioSocket = io.connect();

            return socketFactory({
                ioSocket: ioSocket,
                prefix: ""
            });
        }
    ]);

})();