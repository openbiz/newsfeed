var NewsFeed = angular.module("NewsFeed", ["ngRoute", "ui.bootstrap", "btford.socket-io"]);

(function() {

	NewsFeed.config([
		"$routeProvider", 
		function($routeProvider) {

			$routeProvider
				.when("/", {
					templateUrl: "views/list.html",
					controller: "ListController"
				})
				.when("/post", {
					templateUrl: "views/post.html",
					controller: "PostController"
				})
				.otherwise({
					redirectTo: "/"
				});

		}
	]);

})();