
/**
 * Module dependencies
 */

var express = require('express'),
  routes = require('./routes'),
  api = require('./routes/api'),
  http = require('http'),
  path = require('path');

var app = module.exports = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

/**
 * Configuration
 */

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

// development only
if (app.get('env') === 'development') {
  app.use(express.errorHandler());
}

// production only
if (app.get('env') === 'production') {
  // TODO
};


/**
 * Global variables
 */
db = require("mongojs").connect("newsfeed_app",["users","newsfeed"]);

db.users.find({},function(err,res){
	if(!err && res.length === 0){
		// No users, database wasn't setup
		db.users.insert({
			username: "admin",
			password: "admin",
			display_name: "The Allmighty Administrator",
			permissions: ["read","write","edit"]
		});

		db.users.insert({
			username: "editor",
			password: "editor",
			display_name: "The Underappreciated Editor",
			permissions: ["read","edit"]
		});
		
		db.users.insert({
			username: "intern",
			password: "intern",
			display_name: "The Untrusted Intern",
			permissions: ["read"]
		});
	}
});

/**
 * Routes
 */

// serve index and view partials
app.get('/', routes.index);
app.get('/partials/:name', routes.partials);

// JSON API
app.get('/api/name', api.name);

// redirect all others to the index (HTML5 history)
app.get('*', routes.index);

// Socket.io Communication
io.sockets.on('connection', require('./routes/socket'));

/**
 * Start Server
 */

server.listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
