

/*
 * Serve content over a socket
 */

module.exports = function (socket) {
  var Auth = require("../lib/auth");
  var auth = new Auth();

  // Check a username + password combination
  socket.on("auth:login", function(data){
  	auth.login(data.username, data.password, function(err,session){
  		if(err){
  			// Could not login
  			socket.emit("auth:error", {
  				message: "Could not log the user in: Incorrect username or password"
  			})
  		} else {
  			// Login was succesful 
  			socket.emit("auth:session", {
  				session: session
  			})
  		}
  	});
  });

  // Show the time
  setInterval(function () {
    socket.emit('send:time', {
      time: (new Date()).toString()
    });
  }, 1000);
};
