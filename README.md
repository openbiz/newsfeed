# Simple Newsfeed App

This is a skeleton framework for a simple newsfeed app. Your task will be to update this application so that the following actions become possible:

## Login and retain a session
A socket connection for authentication has been provided in the skeleton framework that logs a user in and returns a session. The socket name is auth:login, and it will return either an auth:error event or an auth:session event, depending on if login was succesful or not.

Three user accounts have been provided:

* admin/admin
* editor/editor
* intern/intern

The admin user is able to post, whereas the others are not. You don't have to be logged in to see the news feed. Each user has a display name associated with it that should be shown on the front page.

## Post a news article
Views have been provided on the front end to enter information into a set of fields. Your task is to associate these fields with a socket event, and to ensure that the user who tries to post something to the news feed is logged in and has permission to do so.

* The article has to be added into the Mongo Database
* The permissions is just an array that has a set of elements from the following options: read, write, edit
* When an article gets posted, it has to show up on every user's screen without having to refresh the page

# What we will look for
Each of you will get a repo to push your code onto. You'll need a bitbucket account to do so. We will assess your code based on how well your code works, whether we can break it, which design patterns you used in both the front and the back end, and the overal quality and readability of the code. We'll also assess the time it took you to complete the main tasks, and give bonus points for any extras you've added, depending on their complexity.

# How to use it

From the project root, run the following commands:

    sudo npm install -g bower
    sudo npm install  
    bower install

You will also need to have MongoDB installed on your PC


To run application, type

    node app.js

from the project root.

## Directory Layout
    
    app.js                  --> app config
    bower.json              --> for bower
    package.json            --> for npm
    public/                 --> all of the files to be used in on the client side
      css/                  --> css files
        index.css             --> default stylesheet
      img/                  --> image files
      bower_components/     --> bower denpendencies
      controllers/          --> app controller files
      factories/
        socket.js           --> to init a socket service
      services/             --> app service files
      views/                --> app partial view files
      app.js                --> app module initialisation
      index.html            --> app main HTML
    routes/
      index.js              --> route for serving HTML pages and partials

## Others

1. Refer to `bower.json` and `package.json` to lookup for modules in use.